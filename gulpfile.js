var gulp = require('gulp');
var uglify = require('gulp-uglify');
var logger = require('gulp-logger');
var sasslint = require('gulp-sass-lint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var filter = require('gulp-filter');
var mainBowerFiles = require('main-bower-files');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');


//var jsDest = 'app/resources/views/wedding/js/bowermin/';
var jsDest = 'web/js/';
var cssDest = 'web/css/';


gulp.task('lintcss', function() {
    return gulp.src('app/resources/views/wedding/css/*.scss')
        .pipe(sasslint())
        .pipe(sasslint.format())
        .pipe(sasslint.failOnError())
});

gulp.task('sass', function(){
    return gulp.src('app/resources/views/wedding/css/*.scss')
        .pipe(sass()) // Converts Sass to CSS with gulp-sass
        .pipe(gulp.dest('web/css'))
});

gulp.task('watch', function () {
    gulp.watch('app/resources/views/wedding/css/*.scss', ['sass'])
});

gulp.task('bowerjs', function() {

    //these have problems when when using the bower repos
    var jsFiles = ['web/js/jquery-migrate-3.0.0.js',    //manually updated
        'web/js/from_bower/jquery.waypoints.min.js',
        'web/js/from_bower/widget.js',
        'web/js/from_bower/jquery.doubletaptogo.min.js',
        'web/js/script.js',
        'web/js/main-slider-fade.js'];

    const jsFilter = filter('**/*.js');

    return gulp.src(mainBowerFiles().concat(jsFiles))
        .pipe(jsFilter)
        .pipe(concat('bowmain.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest));
});

gulp.task('bowercss', ['sass'], function() { //why bowcss - who knows
    var cssFiles = ['web/css/bootstrap/bootstrap.min.css',
        'web/css/custom.css',
        'web/css/preloader.css',
        'web/css/preloader-default.css',
        'web/css/flexslider/flexslider.css',
        'web/css/animate/animate.css',
        'web/css/magnific-popup/magnific-popup.css',
        'web/css/owlcarousel/owl.carousel.css',
        'web/css/owlcarousel/owl.theme.css',
        'web/css/fonts/fontello/css/fontello.css',
        'web/css/style.css'
        ];

    return gulp.src(cssFiles)
        .pipe(concat('bowcss.css'))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(cssDest));
});
