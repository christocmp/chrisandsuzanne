/*
* this node script will convert all images in a folder to the desired format
*
* parameters
* -webp - converts to webp
*/

var imagemin = require("imagemin");    // The imagemin module.
var webp = require("imagemin-webp");   // imagemin's WebP plugin.
var pngImages = "./img/*.png";         // Input folders
var jpegImages = "./img/*.jpg";
var jpeg2Images = "./img/*.jpeg";
var gifImages = "./img/*.gif";
var outputFolder = "./img";            // Output folder


var parameter1 = (process.argv[2]);

switch (parameter1) {
    case undefined:
        console.log('Please enter a valid parameter');
        break;
    case null:
        console.log('Please enter a valid parameter');
        break;
    case 'webp':
        toWebp();
        break;
}

function toWebp() {
    console.log('encoding pngs....');
    imagemin([pngImages], outputFolder, {
        plugins: [webp({
            lossless: true // Losslessly encode images
        })]
    });

    console.log('encoding jpegs....');
    imagemin([jpegImages], outputFolder, {
        plugins: [
            webp({quality: 85})
        ]
    }); /*.then(files => {console.log(files)}); debug if needed*/

    console.log('encoding jpegs....');
    imagemin([jpeg2Images], outputFolder, {
        plugins: [
            webp({quality: 85})
        ]
    });

    console.log('encoding gifs....');
    imagemin([gifImages], outputFolder, {
        plugins: [webp({
            lossless: true // Losslessly encode images
        })]
    });

}
