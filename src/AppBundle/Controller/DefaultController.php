<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Invitation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\InvitationType;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('wedding/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/rsvp", name="rsvp")
     */
    public function rsvpAction(Request $request)
    {
        $invitation = new Invitation();


        $form = $this->createForm(InvitationType::class, $invitation);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $invitation = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($invitation);
            $em->flush();

            return $this->redirectToRoute('rsvp-success');
        }


        return $this->render('wedding/rsvp.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'invitation_form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/rsvp-success", name="rsvp-success")
     */
    public function rsvpSuccessAction(Request $request)
    {

        return $this->render('wedding/rsvp-success.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR
        ]);
    }


    /**
     * @Route("/suzanne", name="suzanne-bio")
     */
    public function suzanneAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('wedding/suzanne.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/christo", name="christo-bio")
     */
    public function christoAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('wedding/christo.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/gallery", name="gallery")
     */
    public function galleryAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('wedding/gallery-prewed.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/location", name="location")
     */
    public function locationAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('wedding/location.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/accomodation", name="accomodation")
     */
    public function accomodationAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('wedding/accomodation.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/wedding-presents", name="wedding-presents")
     */
    public function weddingpresentsAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('wedding/wedding-presents.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }


}
