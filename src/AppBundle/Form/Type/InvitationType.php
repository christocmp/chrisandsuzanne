<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;



class InvitationType extends AbstractType
{
    public function buildForm(
        FormBuilderInterface $builder, array $options
    ) {
        $builder
            ->add('firstname', TextType::class, array('trim' => true, 'attr' => ['class' => 'form-control ajax-input']))
            ->add('lastname', TextType::class, array('trim' => true, 'attr' => ['class' => 'form-control ajax-input']))
            ->add('guests', ChoiceType::class, array('attr' => ['class' => 'form-control ajax-input'],
                    'choices' => array(
                        '1' => 1,
                        '2' => 2,
                        '3' => 3,
                        '4' => 4,
                        '5' => 5),

                )
            )
            ->add('notes', TextareaType::class, array('required' => false, 'attr' => ['class' => 'form-control ajax-input', 'rows' => '10']))
           // ->add('notes', TextType::class, array('attr' => ['class' => 'form-control ajax-input']))
           // ->add('registration_date', DateType::class)
            ->add('attending', ChoiceType::class, array('required' => true,
                'choices' => array(
                    'Wedding only' => 2,
                    'Wedding & reception' => 3,
                    'Reception only' => 1,
                    "Unable to attend \xF0\x9F\x98\x9E" => 0),
               'multiple' => false,
               'expanded' => true,
               'data' => 3


                )
            )
            ->add('email', TextType::class, array('required' => true, 'trim'=> true, 'attr' => ['class' => 'form-control ajax-input']))
            ->add('telephone', TextType::class, array('required' => false, 'attr' => ['class' => 'form-control ajax-input']))
            ->add('save', SubmitType::class, array('label' => 'Submit', 'attr' => ['class' => 'rsvpsubmit btn-5']));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Invitation',
        ]);
    }

    public function getName()
    {
        return 'invitation';
    }
}